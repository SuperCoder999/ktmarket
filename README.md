# KTMarket

Marketplace written in Kotlin

## Installation

- Clone the repository
- Install Android SDK, JDK and Kotlin SDK (or Android Studio)

## Start

- `flutter emulator --launch <your_avd_name>` or something similar
- Open project in Android Studio
- Sync Gradle (only first time)
- Shift + F10 (or run button in the header)

## Notes

- Here product can have only one image
- I've used my own **GraphQL server**
- I've written my own client for this, so I've learned REST too :)
- I've used MVVM architecture, where Models are Repositories, which return DTOs
- Don't ask about these big buttons :) Android Studio forces me to set `minHeight="48dp"`
- Minimum Android version is 26
