package com.example.marketplace.exceptions

import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

class GraphQLException(json: JSONArray)
    : Exception(this.jsonArrayExceptionToString(json)) {
    companion object {
        private fun jsonArrayExceptionToString(array: JSONArray): String {
            val messages: MutableList<String> = mutableListOf()

            for (i in 0 until array.length()) {
                val obj: JSONObject = array.getJSONObject(i)
                messages.add(obj.getString("message"))
            }

            return messages.joinToString(separator = ", ")
        }
    }
}