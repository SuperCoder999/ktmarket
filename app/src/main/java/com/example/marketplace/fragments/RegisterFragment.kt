package com.example.marketplace.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.esafirm.imagepicker.model.Image
import com.example.marketplace.R
import com.example.marketplace.dto.auth.UserRegisterDto
import com.example.marketplace.extensions.isValid
import com.example.marketplace.extensions.text
import com.example.marketplace.helpers.ImageHelper
import com.example.marketplace.images.SingleImagePicker
import com.example.marketplace.validation.DefaultValidators
import com.example.marketplace.validation.InputValidationWatcher
import com.example.marketplace.viewModels.AuthViewModel
import com.example.marketplace.viewModels.ViewModelDelegate
import kotlinx.android.synthetic.main.register_fragment.*

class RegisterFragment : Fragment() {
    private val viewModel by ViewModelDelegate(AuthViewModel::class.java)
    private lateinit var navigator: NavController
    private lateinit var avatarPicker: SingleImagePicker
    private var avatar: Image? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator = findNavController()

        avatarPicker = SingleImagePicker(this) {
            if (this == null) {
                avatar = null
                registerAvatar.setImageResource(0) // Remove image
            } else {
                avatar = this
                registerAvatar.setImageBitmap(ImageHelper.imageToBitmap(this))
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()

        registerName.editText
            ?.addTextChangedListener(
                InputValidationWatcher(DefaultValidators.exists, registerName))

        registerEmail.editText
            ?.addTextChangedListener(
                InputValidationWatcher(DefaultValidators.email, registerEmail))

        registerPhoneNumber.editText
            ?.addTextChangedListener(
                InputValidationWatcher(DefaultValidators.phoneNumber, registerPhoneNumber))

        registerPassword.editText
            ?.addTextChangedListener(
                InputValidationWatcher(DefaultValidators.min8, registerPassword))

        registerPickAvatar.setOnClickListener { avatarPicker.pick() }
        registerButton.setOnClickListener { submit() }
        loginRedirect.setOnClickListener { navigator.navigate(R.id.login) }
    }

    private fun submit() {
        val valid: Boolean = registerName.isValid
                && registerEmail.isValid
                && registerPhoneNumber.isValid
                && registerPassword.isValid

        if (!valid) {
            return
        }

        showLoader()

        val observable: LiveData<Void> = viewModel
            .register(UserRegisterDto(
                registerName.text,
                registerEmail.text,
                registerPhoneNumber.text,
                registerPassword.text,
                avatar
            ))

        observable
            .observe(this, object : Observer<Void> {
                override fun onChanged(t: Void?) {
                    hideLoader()
                    observable.removeObserver(this)
                }
            })
    }

    private fun showLoader() {
        registerLoader.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        registerLoader.visibility = View.GONE
    }
}