package com.example.marketplace.fragments

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import com.example.marketplace.R
import com.example.marketplace.viewModels.AuthViewModel
import com.example.marketplace.viewModels.ViewModelDelegate
import java.lang.Exception

class NavigationHost : NavHostFragment() {
    private val viewModel by ViewModelDelegate(AuthViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        watchUser()
    }

    private fun watchUser() {
        viewModel.getUser().observe(this) {
            when (it) {
                null -> navigate(R.id.login)
                else -> navigate(R.id.products)
            }
        }
    }

    private fun navigate(res: Int) {
        if (isNotCurrentDestination(res)) {
            navController.navigate(res)
        }
    }

    private fun isNotCurrentDestination(res: Int): Boolean {
        return navController.currentDestination?.id != res
    }
}