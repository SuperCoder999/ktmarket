package com.example.marketplace.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.marketplace.R
import com.example.marketplace.extensions.text
import com.example.marketplace.viewComponents.ProductsListRecyclerAdapter
import com.example.marketplace.viewModels.AuthViewModel
import com.example.marketplace.viewModels.ProductsViewModel
import com.example.marketplace.viewModels.ViewModelDelegate
import kotlinx.android.synthetic.main.products_fragment.*

class ProductsFragment : Fragment() {
    private val viewModel by ViewModelDelegate(ProductsViewModel::class.java)
    private val authViewModel by ViewModelDelegate(AuthViewModel::class.java)
    private lateinit var listAdapter: ProductsListRecyclerAdapter
    private lateinit var navigator: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator = findNavController()
    }

    override fun onStart() {
        super.onStart()

        listAdapter = ProductsListRecyclerAdapter(
            requireContext(),
            resources,
            listOf(),
            { id -> loadExpanded(id) },
            { id -> addToCart(id) },
            { id -> removeFromCart(id) }
        )

        watchLoading()
        watchProducts()
        watchExpanded()

        productsList.adapter = listAdapter

        productsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val maxScroll: Int = recyclerView.computeVerticalScrollRange()
                val currentScroll: Int = recyclerView.computeVerticalScrollOffset()
                val currentVisible: Int = recyclerView.computeVerticalScrollExtent()
                val scrollDiff = maxScroll - currentScroll - currentVisible

                if (scrollDiff <= 10) {
                    viewModel.loadMoreProducts()
                }
            }
        })

        productsLogout.setOnClickListener { authViewModel.logout() }
        productsCreate.setOnClickListener { navigator.navigate(R.id.createProduct) }
        productsSwitchMode.setOnClickListener { navigator.navigate(R.id.cart) }
        productsSearchButton.setOnClickListener { search() }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.products_fragment, container, false)
    }

    private fun watchLoading() {
        viewModel.getLoading().observe(this) {
            productsLoader.visibility = if (it) View.VISIBLE else View.GONE
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun watchProducts() {
        viewModel.getProducts().observe(this) {
            listAdapter.products = List(it.size) { index -> it[index] }
            listAdapter.notifyDataSetChanged()
        }
    }

    private fun watchExpanded() {
        viewModel.getExpandedProduct().observe(this) {
            if (it != null) {
                navigator.navigate(R.id.expandedProduct)
            }
        }
    }

    private fun loadExpanded(id: String) {
        viewModel.loadExpandedProduct(id)
    }

    private fun addToCart(id: String) {
        viewModel.addToCart(id)
    }

    private fun removeFromCart(id: String) {
        viewModel.removeFromCart(id)
    }

    private fun search() {
        val value: String = productsSearch.text
        viewModel.reloadProducts(value)
    }
}