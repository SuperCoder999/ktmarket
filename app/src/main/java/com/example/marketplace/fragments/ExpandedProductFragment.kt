package com.example.marketplace.fragments

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.marketplace.R
import com.example.marketplace.dto.auth.UserDto
import com.example.marketplace.dto.products.ProductDto
import com.example.marketplace.helpers.ImageHelper
import com.example.marketplace.helpers.LinkingHelper
import com.example.marketplace.viewModels.AuthViewModel
import com.example.marketplace.viewModels.ProductsViewModel
import com.example.marketplace.viewModels.ViewModelDelegate
import kotlinx.android.synthetic.main.expanded_product_fragment.*

class ExpandedProductFragment : Fragment() {
    private val authViewModel by ViewModelDelegate(AuthViewModel::class.java)
    private val viewModel by ViewModelDelegate(ProductsViewModel::class.java)
    private lateinit var navigator: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator = findNavController()

        watchLoading()
        watchProduct()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.expanded_product_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()
        expandedProductBackButton.setOnClickListener { viewModel.removeExpandedProduct() }
    }

    private fun watchProduct() {
        viewModel.getExpandedProduct().observe(this) {
            if (it == null) {
                goBack()
            } else {
                applyProductData(it)
            }
        }
    }

    private fun watchLoading() {
        viewModel.getLoading().observe(this) {
            expandedProductLoader.visibility = if (it) View.VISIBLE else View.GONE
        }
    }

    @SuppressLint("SetTextI18n")
    private fun applyProductData(product: ProductDto) {
        if (product.picture == null) {
            expandedProductImage.visibility = View.GONE
        } else {
            val image: Bitmap? = ImageHelper.getImageFromNetwork(product.picture!!.url)
            expandedProductImage.setImageBitmap(image)
            expandedProductImage.visibility = View.VISIBLE
        }

        if (product.seller.avatar == null) {
            expandedProductSellerAvatar.visibility = View.GONE
        } else {
            val image: Bitmap? = ImageHelper.getImageFromNetwork(product.seller.avatar!!.url)
            expandedProductSellerAvatar.setImageBitmap(image)
            expandedProductSellerAvatar.visibility = View.VISIBLE
        }

        expandedProductTitle.text = product.title
        expandedProductPrice.text = "${product.price}$"
        expandedProductDescription.text = product.description
        expandedProductSellerName.text = product.seller.name
        expandedProductSellerPhone.text = product.seller.phoneNumber

        if (product.seller.id == authViewModel.getUser().value!!.id) {
            expandedProductPrimaryButton.text = resources.getString(R.string.edit)
            expandedProductPrimaryButton.setOnClickListener { navigator.navigate(R.id.editProduct) }

            expandedProductSecondaryButton.text = resources.getString(R.string.delete)
            expandedProductSecondaryButton.setOnClickListener { viewModel.delete() } // Delete expanded
        } else {
            expandedProductPrimaryButton.text = resources.getString(
                if (product.isInCart) R.string.remove_from_cart else R.string.add_to_cart
            )

            expandedProductPrimaryButton.setOnClickListener {
                if (product.isInCart) removeFromCart(product.id) else addToCart(product.id)
            }

            expandedProductSecondaryButton.text = resources.getText(R.string.call_seller)

            expandedProductSecondaryButton.setOnClickListener {
                LinkingHelper.makeCall(this, product.seller.phoneNumber)
            }
        }
    }

    private fun addToCart(id: String) {
        viewModel.addToCart(id)
    }

    private fun removeFromCart(id: String) {
        viewModel.removeFromCart(id)
    }

    private fun goBack() {
        navigator.navigate(R.id.products)
    }
}