package com.example.marketplace.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.esafirm.imagepicker.model.Image
import com.example.marketplace.R
import com.example.marketplace.dto.products.ProductDto
import com.example.marketplace.dto.products.ProductUpdateDto
import com.example.marketplace.extensions.isValid
import com.example.marketplace.extensions.text
import com.example.marketplace.helpers.ImageHelper
import com.example.marketplace.images.SingleImagePicker
import com.example.marketplace.validation.DefaultValidators
import com.example.marketplace.validation.InputValidationWatcher
import com.example.marketplace.viewModels.ProductsViewModel
import com.example.marketplace.viewModels.ViewModelDelegate
import kotlinx.android.synthetic.main.create_product_fragment.*

class UpdateProductFragment : Fragment() {
    private val viewModel by ViewModelDelegate(ProductsViewModel::class.java)
    private lateinit var navigator: NavController
    private lateinit var imagePicker: SingleImagePicker
    private lateinit var product: ProductDto
    private var newImage: Image? = null
    private var oldImageDeleted: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator = findNavController()

        imagePicker = SingleImagePicker(this) {
            if (this == null) {
                newImage = null
                createProductImage.setImageResource(0)
            } else {
                newImage = this
                createProductImage.setImageBitmap(ImageHelper.imageToBitmap(this))
            }

            oldImageDeleted = true
        }

        watchLoading()
        watchExpanded()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.create_product_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()

        createProductTitle.editText
            ?.addTextChangedListener(
                InputValidationWatcher(DefaultValidators.exists, createProductTitle))

        createProductPrice.editText
            ?.addTextChangedListener(
                InputValidationWatcher(DefaultValidators.positive, createProductPrice))

        createProductPickImage.setOnClickListener { imagePicker.pick() }
        createProductSubmit.setOnClickListener { submit() }
        createProductBackButton.setOnClickListener { goBack() }
    }

    private fun submit() {
        val valid: Boolean = createProductTitle.isValid && createProductPrice.isValid

        if (!valid) {
            return
        }

        val observable: LiveData<Void> = viewModel
            .update(ProductUpdateDto(
                product.id,
                createProductTitle.text,
                createProductPrice.text.toDouble(),
                createProductDescription.text,
                newImage,
                if (oldImageDeleted) product.picture?.id else null
            ))

        observable.observe(this, object : Observer<Void> {
            override fun onChanged(t: Void?) {
                observable.removeObserver(this)
                goBack()
            }
        })
    }

    private fun watchLoading() {
        viewModel.getLoading().observe(this) {
            createProductLoader.visibility = if (it) View.VISIBLE else View.GONE
        }
    }

    private fun watchExpanded() {
        viewModel.getExpandedProduct().observe(this) {
            if (it == null) {
                goBack()
            } else {
                applyExpandedProduct(it)
            }
        }
    }

    private fun applyExpandedProduct(product: ProductDto) {
        this.product = product

        createProductTitle.text = product.title
        createProductPrice.text = product.price.toString()
        createProductDescription.text = product.description
        newImage = null

        if (product.picture == null) {
            createProductImage.setImageResource(0) // Remove
        } else {
            createProductImage.setImageBitmap(ImageHelper.getImageFromNetwork(product.picture!!.url))
        }

        oldImageDeleted = false
    }

    private fun goBack() {
        navigator.navigate(R.id.expandedProduct)
    }
}