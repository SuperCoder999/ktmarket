package com.example.marketplace.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.marketplace.R
import com.example.marketplace.viewComponents.ProductsListRecyclerAdapter
import com.example.marketplace.viewModels.ProductsViewModel
import com.example.marketplace.viewModels.ViewModelDelegate
import kotlinx.android.synthetic.main.cart_fragment.*

class CartFragment : Fragment() {
    private val viewModel by ViewModelDelegate(ProductsViewModel::class.java)
    private lateinit var listAdapter: ProductsListRecyclerAdapter
    private lateinit var navigator: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator = findNavController()
    }

    override fun onStart() {
        super.onStart()

        listAdapter = ProductsListRecyclerAdapter(
            requireContext(),
            resources,
            listOf(),
            { id -> loadExpanded(id) },
            { id -> addToCart(id) },
            { id -> removeFromCart(id) }
        )

        watchLoading()
        watchCart()
        watchExpanded()

        cartList.adapter = listAdapter
        cartSwitchMode.setOnClickListener { navigator.navigate(R.id.products) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.cart_fragment, container, false)
    }

    private fun watchLoading() {
        viewModel.getLoading().observe(this) {
            cartLoader.visibility = if (it) View.VISIBLE else View.GONE
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun watchCart() {
        viewModel.getCart().observe(this) {
            listAdapter.products = List(it.size) { index -> it[index] }
            listAdapter.notifyDataSetChanged()
        }
    }

    private fun watchExpanded() {
        viewModel.getExpandedProduct().observe(this) {
            if (it != null) {
                navigator.navigate(R.id.expandedProduct)
            }
        }
    }

    private fun loadExpanded(id: String) {
        viewModel.loadExpandedProduct(id)
    }

    private fun addToCart(id: String) {
        viewModel.addToCart(id)
    }

    private fun removeFromCart(id: String) {
        viewModel.removeFromCart(id)
    }
}