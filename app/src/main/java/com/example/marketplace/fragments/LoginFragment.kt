package com.example.marketplace.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.marketplace.R
import com.example.marketplace.dto.auth.UserCredentialsDto
import com.example.marketplace.extensions.isValid
import com.example.marketplace.extensions.text
import com.example.marketplace.validation.DefaultValidators
import com.example.marketplace.validation.InputValidationWatcher
import com.example.marketplace.viewModels.AuthViewModel
import com.example.marketplace.viewModels.ViewModelDelegate
import kotlinx.android.synthetic.main.login_fragment.*

class LoginFragment : Fragment() {
    private val viewModel by ViewModelDelegate(AuthViewModel::class.java)
    private lateinit var navigator: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator = findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()

        loginEmail.editText
            ?.addTextChangedListener(
                InputValidationWatcher(DefaultValidators.email, loginEmail))

        loginPassword.editText
            ?.addTextChangedListener(
                InputValidationWatcher(DefaultValidators.exists, loginPassword))

        loginButton.setOnClickListener { submit() }
        registerRedirect.setOnClickListener { navigator.navigate(R.id.register) }
    }

    private fun submit() {
        val valid: Boolean = loginEmail.isValid && loginPassword.isValid

        if (!valid) {
            return
        }

        showLoader()

        val observable: LiveData<Void> = viewModel
            .login(UserCredentialsDto(
                loginEmail.text,
                loginPassword.text
            ))

        observable.observe(this, object: Observer<Void> {
            override fun onChanged(t: Void?) {
                hideLoader()
                observable.removeObserver(this)
            }
        })
    }

    private fun showLoader() {
        loginLoader.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        loginLoader.visibility = View.GONE
    }
}