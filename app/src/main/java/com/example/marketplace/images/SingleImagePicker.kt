package com.example.marketplace.images

import androidx.fragment.app.Fragment
import com.esafirm.imagepicker.features.ImagePickerConfig
import com.esafirm.imagepicker.features.ImagePickerMode
import com.esafirm.imagepicker.features.registerImagePicker
import com.esafirm.imagepicker.model.Image
import com.example.marketplace.R

class SingleImagePicker(fragment: Fragment, private val onSelected: Image?.() -> Unit) {
    private val launcher = fragment.registerImagePicker {
        onSelected(if (it.isEmpty()) null else it[0])
    }

    private val config = ImagePickerConfig {
        mode = ImagePickerMode.SINGLE
        language = "en"
        theme = R.style.AppTheme
    }

    fun pick() {
        launcher.launch(config)
    }
}