package com.example.marketplace.graphql.queries

object ProductQueries {
    val getProducts: String = """
        query(${"$"}page: Int!, ${"$"}search: String!) {
            products(page: ${"$"}page, perPage: 40, search: ${"$"}search) {
                hasMoreProducts
                products {
                    id
                    title
                    price
                    description
                    isInCart
                    sellerId
                    pictures {
                        id
                        url
                    }
                }
            }
        }
    """.trimIndent()

    val getProduct: String = """
        query(${"$"}id: ID!) {
            product(id: ${"$"}id) {
                id
                title
                title
                price
                description
                isInCart
                seller {
                    id
                    name
                    phoneNumber
                    avatar {
                        id
                        url
                    }
                }
                pictures {
                    id
                    url
                }
            }
        }
    """.trimIndent()

    val create: String = """
        mutation(${"$"}title: String!, ${"$"}price: Float!, ${"$"}description: String!, ${"$"}pictures: [File!]) {
            createProduct(title: ${"$"}title, price: ${"$"}price, description: ${"$"}description, pictures: ${"$"}pictures) {
                id
                title
                title
                price
                description
                isInCart
                seller {
                    id
                    name
                    phoneNumber
                    avatar {
                        id
                        url
                    }
                }
                pictures {
                    id
                    url
                }
            }
        }
    """.trimIndent()

    val update: String = """
        mutation(${"$"}id: ID!, ${"$"}title: String, ${"$"}price: Float, ${"$"}description: String, ${"$"}pictures: [File!], ${"$"}deletedPictures: [ID!]) {
            updateProduct(id: ${"$"}id, title: ${"$"}title, price: ${"$"}price, description: ${"$"}description, pictures: ${"$"}pictures, deletedPictures: ${"$"}deletedPictures) {
                id
                title
                title
                price
                description
                isInCart
                seller {
                    id
                    name
                    phoneNumber
                    avatar {
                        id
                        url
                    }
                }
                pictures {
                    id
                    url
                }
            }
        }
    """.trimIndent()

    val delete: String = """
        mutation(${"$"}id: ID!) {
            deleteProduct(id: ${"$"}id)
        }
    """.trimIndent()
}