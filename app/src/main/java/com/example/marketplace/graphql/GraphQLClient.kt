package com.example.marketplace.graphql

import com.example.marketplace.exceptions.GraphQLException
import com.example.marketplace.extensions.readInputStream
import com.example.marketplace.storage.LocalStorage
import com.example.marketplace.viewModels.ToastViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.OutputStreamWriter
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.suspendCoroutine

class GraphQLClient(urlString: String) {
    private val url: URL = URL(urlString)

    suspend fun send(query: String, variables: Map<String, Any?> = mapOf()): JSONObject? {
        return withContext(Dispatchers.IO) { // Run in background
            val token: String? = LocalStorage.read(LocalStorage.Keys.userToken)

            suspendCoroutine {
                val obj: JSONObject? = sendSync(query, variables, token)
                it.resumeWith(Result.success(obj))
            }
        }
    }

    private fun sendSync(query: String, variables: Map<String, Any?>, token: String?): JSONObject? {
        with(url.openConnection() as HttpURLConnection) {
            requestMethod = "POST"
            doInput = true
            doOutput = true

            setRequestProperty("Content-Type", "application/json")

            if (token != null) {
                setRequestProperty("Authorization", "Bearer $token")
            }

            val json = JSONObject()
            json.put("query", query)
            json.put("variables", JSONObject(variables))

            val body = OutputStreamWriter(outputStream)
            body.write(json.toString())
            body.flush()

            val responseJson = JSONObject(readInputStream())

            if (!responseJson.isNull("errors")) {
                val exc = GraphQLException(responseJson.getJSONArray("errors"))
                ToastViewModel.addToast(exc.message ?: "")
                return null
            }

            if (responseJson.isNull("data")) {
                return null
            }

            return responseJson.getJSONObject("data")
        }
    }
}