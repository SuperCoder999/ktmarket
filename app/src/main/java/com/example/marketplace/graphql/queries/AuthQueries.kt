package com.example.marketplace.graphql.queries

object AuthQueries {
    val getUser: String = """
        query {
            user {
                id
            }
        }
    """.trimIndent()

    val login: String = """
        mutation(${"$"}email: String!, ${"$"}password: String!) {
            login(email: ${"$"}email, password: ${"$"}password) {
                id
                token
            }
        }
    """.trimIndent()

    val register: String = """
        mutation(${"$"}name: String!, ${"$"}email: String!, ${"$"}phoneNumber: String!, ${"$"}password: String!, ${"$"}avatar: File) {
            register(name: ${"$"}name, email: ${"$"}email, phoneNumber: ${"$"}phoneNumber, password: ${"$"}password, avatar: ${"$"}avatar) {
                id
                token
            }
        }
    """.trimIndent()
}