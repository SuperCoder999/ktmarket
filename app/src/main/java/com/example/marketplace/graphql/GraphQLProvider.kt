package com.example.marketplace.graphql

object GraphQLProvider {
    private const val url = "https://graphmarket-server.herokuapp.com"
    val client = GraphQLClient(url)
}