package com.example.marketplace.graphql.queries

object CartQueries {
    val getCart = """
        query {
            cart {
                id
                title
                price
                description
                isInCart
                sellerId
                pictures {
                    id
                    url
                }
            }
        }
    """.trimIndent()

    val addToCart = """
        mutation(${"$"}id: ID!) {
            addToCart(id: ${"$"}id) {
                id
                title
                title
                price
                description
                isInCart
                seller {
                    id
                    name
                    phoneNumber
                    avatar {
                        id
                        url
                    }
                }
                pictures {
                    id
                    url
                }
            }
        }
    """.trimIndent()

    val removeFromCart = """
        mutation(${"$"}id: ID!) {
            removeFromCart(id: ${"$"}id)
        }
    """.trimIndent()
}