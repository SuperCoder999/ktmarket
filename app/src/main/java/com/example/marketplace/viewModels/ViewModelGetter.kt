package com.example.marketplace.viewModels

import androidx.lifecycle.ViewModel
import java.lang.Exception

object ViewModelGetter {
    private val instances: MutableMap<String, in ViewModel> = mutableMapOf()

    fun <T : ViewModel>get(cls: Class<T>): T {
        val name: String = cls.canonicalName
            ?: throw Exception("ViewModel can't be anonymous")

        if (instances.containsKey(name)) {
            return instances[name] as T
        }

        val instance: T = cls.newInstance()
        instances[name] = instance

        return instance
    }

    fun <T : ViewModel>forceRemoveInstance(cls: Class<T>) {
        val name: String = cls.canonicalName
            ?: throw Exception("ViewModel can't be anonymous")

        instances.remove(name)
    }
}