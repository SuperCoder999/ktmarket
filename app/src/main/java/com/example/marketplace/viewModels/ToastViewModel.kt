package com.example.marketplace.viewModels

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ToastViewModel : ViewModel() {
    companion object {
        private val instances: MutableList<ToastViewModel> = mutableListOf()

        fun addToast(text: String) {
            instances.forEach {
                it.addToast(text)
            }
        }
    }

    init {
        instances.add(this)
    }

    private val toastData: MutableLiveData<String> = MutableLiveData<String>()

    fun addToast(text: String) {
        viewModelScope.launch {
            withContext(Dispatchers.Main) { toastData.value = text }
        }
    }

    fun getToast(): LiveData<String> {
        return toastData
    }

    fun showToast(context: Context, text: String) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }
}