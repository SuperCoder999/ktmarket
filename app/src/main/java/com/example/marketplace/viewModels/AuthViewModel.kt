package com.example.marketplace.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.marketplace.dto.auth.UserCredentialsDto
import com.example.marketplace.dto.auth.UserDto
import com.example.marketplace.dto.auth.UserRegisterDto
import com.example.marketplace.extensions.notifyListeners
import com.example.marketplace.repositories.AuthRepository
import kotlinx.coroutines.launch

class AuthViewModel : ViewModel() {
    private val user: MutableLiveData<UserDto?> = MutableLiveData<UserDto?>()
    private val loginSuccess: MutableLiveData<Void> = MutableLiveData<Void>()
    private val registerSuccess: MutableLiveData<Void> = MutableLiveData<Void>()

    init {
        viewModelScope.launch { loadUser() }
    }

    fun getUser(): LiveData<UserDto?> {
        return user;
    }

    fun login(data: UserCredentialsDto): LiveData<Void> {
        viewModelScope.launch {
            val newUser: UserDto? = AuthRepository.login(data)
            user.value = newUser
            loginSuccess.notifyListeners()
        }

        return loginSuccess
    }

    fun register(data: UserRegisterDto): LiveData<Void> {
        viewModelScope.launch {
            val newUser: UserDto? = AuthRepository.register(data)
            user.value = newUser
            registerSuccess.notifyListeners()
        }

        return registerSuccess
    }

    fun logout() {
        viewModelScope.launch {
            AuthRepository.logout()
            user.value = null
        }
    }

    private suspend fun loadUser() {
        val newUser: UserDto? = AuthRepository.getUser()
        user.value = newUser
    }
}