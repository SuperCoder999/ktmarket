package com.example.marketplace.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.marketplace.dto.products.*
import com.example.marketplace.extensions.notifyListeners
import com.example.marketplace.extensions.removeWhere
import com.example.marketplace.extensions.replaceWhere
import com.example.marketplace.repositories.CartRepository
import com.example.marketplace.repositories.ProductRepository
import kotlinx.coroutines.launch

class ProductsViewModel : ViewModel() {
    private var filter: ProductFilterDto = ProductFilterDto()
    private var hasMoreProducts: Boolean = false
    private val loading: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    private val products: MutableLiveData<List<ProductListDto>> =
        MutableLiveData<List<ProductListDto>>(listOf())

    private val cart: MutableLiveData<List<ProductListDto>> =
        MutableLiveData<List<ProductListDto>>(listOf())

    private val expandedProduct: MutableLiveData<ProductDto?> = MutableLiveData<ProductDto?>()
    private val createProductSuccess: MutableLiveData<Void> = MutableLiveData<Void>()
    private val updateProductSuccess: MutableLiveData<Void> = MutableLiveData<Void>()

    init {
        reloadProducts()
        loadCart()
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    fun getProducts(): LiveData<List<ProductListDto>> {
        return products
    }

    fun getCart(): LiveData<List<ProductListDto>> {
        return cart
    }

    fun getExpandedProduct(): LiveData<ProductDto?> {
        return expandedProduct
    }

    fun loadMoreProducts() {
        if (!hasMoreProducts) {
            return
        }

        filter.page += 1
        loading.value = true

        viewModelScope.launch {
            val list: ProductListInfoDto? = ProductRepository.getProducts(filter)

            if (list != null) {
                products.value = products.value!! + list.products
                hasMoreProducts = list.hasMoreProducts
            }

            loading.value = false
        }
    }

    fun reloadProducts(search: String = "") {
        filter = ProductFilterDto()
        filter.search = search
        loading.value = true

        viewModelScope.launch {
            val list: ProductListInfoDto? = ProductRepository.getProducts(filter)

            if (list != null) {
                products.value = list.products
                hasMoreProducts = list.hasMoreProducts
            }

            loading.value = false
        }
    }

    fun loadExpandedProduct(id: String) {
        loading.value = true

        viewModelScope.launch {
            val product: ProductDto? = ProductRepository.getProduct(id)

            if (product != null) {
                expandedProduct.value = product
            }

            loading.value = false
        }
    }

    fun removeExpandedProduct() {
        expandedProduct.value = null
    }

    fun create(data: ProductCreateDto): LiveData<Void> {
        loading.value = true

        viewModelScope.launch {
            val product: ProductDto? = ProductRepository.create(data)

            if (product != null) {
                products.value = listOf(ProductListDto.fromFull(product)) + products.value!!
                createProductSuccess.notifyListeners()
            }

            loading.value = false
        }

        return createProductSuccess
    }

    fun update(data: ProductUpdateDto): LiveData<Void> {
        loading.value = true

        viewModelScope.launch {
            val product: ProductDto? = ProductRepository.update(data)

            if (product != null) {
                val listDto: ProductListDto = ProductListDto.fromFull(product)

                products.value = products.value!!.replaceWhere(listDto) { id == product.id }
                cart.value = cart.value!!.replaceWhere(listDto) { id == product.id }

                if (expandedProduct.value?.id == product.id) {
                    expandedProduct.value = product
                }

                updateProductSuccess.notifyListeners()
            }

            loading.value = false
        }

        return updateProductSuccess
    }

    fun delete() {
        if (expandedProduct.value == null) {
            return
        }

        loading.value = true

        viewModelScope.launch {
            val removeId: String? = ProductRepository.delete(expandedProduct.value!!.id)

            if (removeId != null) {
                products.value = products.value!!.removeWhere { id == removeId }
                cart.value = cart.value!!.removeWhere { id == removeId }
                expandedProduct.value = null
            }

            loading.value = false
        }
    }

    fun addToCart(id: String) {
        loading.value = true

        viewModelScope.launch {
            val product: ProductDto? = CartRepository.addToCart(id)

            if (product != null) {
                product.isInCart = true
                val listDto = ProductListDto.fromFull(product)

                products.value = products.value!!.replaceWhere(listDto) { this.id == product.id }
                cart.value = listOf(listDto) + cart.value!!

                if (expandedProduct.value?.id == product.id) {
                    expandedProduct.value = product
                }
            }

            loading.value = false
        }
    }

    fun removeFromCart(id: String) {
        loading.value = true

        viewModelScope.launch {
            val removeId: String? = CartRepository.removeFromCart(id)

            if (removeId != null) {
                products.value = products.value!!.replaceWhere<ProductListDto>({
                    val copied = copy()
                    copied.isInCart = false
                    copied
                }) { this.id == removeId }

                cart.value = cart.value!!.removeWhere { this.id == removeId }

                if (expandedProduct.value?.id == removeId) {
                    val newExpanded: ProductDto = expandedProduct.value!!.copy()
                    newExpanded.isInCart = false
                    expandedProduct.value = newExpanded
                }
            }

            loading.value = false
        }
    }

    private fun loadCart() {
        loading.value = true

        viewModelScope.launch {
            val list: List<ProductListDto>? = CartRepository.getCart()

            if (list != null) {
                cart.value = list!!
            }

            loading.value = false
        }
    }
}