package com.example.marketplace.viewModels

import androidx.lifecycle.ViewModel
import kotlin.reflect.KProperty

class ViewModelDelegate<T : ViewModel>(private val cls: Class<T>) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return ViewModelGetter.get(cls)
    }
}