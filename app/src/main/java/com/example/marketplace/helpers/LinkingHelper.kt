package com.example.marketplace.helpers

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment

object LinkingHelper {
    fun makeCall(owner: Fragment, phone: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phone")
        owner.startActivity(intent)
    }
}