package com.example.marketplace.helpers

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.esafirm.imagepicker.model.Image
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.File
import java.net.HttpURLConnection
import java.net.URL

object ImageHelper {
    fun imageToBitmap(image: Image): Bitmap {
        val bytes: ByteArray = File(image.path).readBytes()
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    }

    fun getImageFromNetwork(urlString: String): Bitmap? {
        val url = URL(urlString)

        return runBlocking {
            withContext(Dispatchers.IO) {
                with(url.openConnection() as HttpURLConnection) {
                    requestMethod = "GET"
                    doInput = true

                    if (responseCode in 200..399) {
                        BitmapFactory
                            .decodeStream(inputStream, null, BitmapFactory.Options())
                    } else {
                        null
                    }
                }
            }
        }
    }
}