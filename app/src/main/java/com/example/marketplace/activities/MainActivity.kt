package com.example.marketplace.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.marketplace.R
import com.example.marketplace.viewModels.ToastViewModel

class MainActivity : AppCompatActivity() {
    private val toastViewModel by viewModels<ToastViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        watchToasts()
    }

    private fun watchToasts() {
        toastViewModel.getToast().observe(this) {
            toastViewModel.showToast(applicationContext, it)
        }
    }
}