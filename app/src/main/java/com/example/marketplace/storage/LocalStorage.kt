package com.example.marketplace.storage

import android.os.Build
import android.os.Environment
import androidx.annotation.RequiresApi
import com.example.marketplace.BuildConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.File
import java.security.Key
import kotlin.io.path.createDirectories

object LocalStorage {
    object Keys {
        const val userToken = "auth:token"
    }

    private const val storageFileName = "storage.json"
    private val directory: File = Environment.getDataDirectory()
    private val file: File = File(
        "${directory.path}/data/${BuildConfig.APPLICATION_ID}/databases/$storageFileName")
    private var cache: JSONObject? = null

    suspend fun read(name: String): String? {
        fetchCache()

        if (cache!!.isNull(name)) {
            return null
        }

        return cache!!.getString(name)
    }

    suspend fun write(name: String, value: String) {
        fetchCache()
        cache!!.put(name, value)
        pushCache()
    }

    suspend fun remove(name: String) {
        fetchCache()
        cache!!.remove(name)
        pushCache()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private suspend fun fetchCache() {
        if (cache == null) {
            withContext(Dispatchers.IO) {
                if (!file.exists()) {
                    cache = JSONObject()
                    file.parentFile.toPath().createDirectories()
                    file.createNewFile()
                    pushCache()
                } else {
                    val text = file.readText()
                    val jsonText = text.ifEmpty { "{}" }
                    cache = JSONObject(jsonText)
                }
            }
        }
    }

    private suspend fun pushCache() {
        withContext(Dispatchers.IO) {
            file.writeText(cache.toString())
        }
    }
}