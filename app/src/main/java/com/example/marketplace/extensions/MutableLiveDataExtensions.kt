package com.example.marketplace.extensions

import androidx.lifecycle.MutableLiveData

fun MutableLiveData<in Nothing>.notifyListeners() {
    this.value = this.value
}