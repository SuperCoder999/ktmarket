package com.example.marketplace.extensions

import java.io.InputStream
import java.net.HttpURLConnection

fun HttpURLConnection.readInputStream(): String {
    val stream: InputStream = if (responseCode in 200..299) inputStream else errorStream

    return stream.bufferedReader().useLines {
        it.joinToString("\n")
    }
}