package com.example.marketplace.extensions

import com.esafirm.imagepicker.model.Image
import java.io.File
import java.util.*

fun Image.toMap(): Map<String, Any?> {
    val bytes: ByteArray = File(path).readBytes()

    return mapOf(
        "filename" to name,
        "base64" to Base64.getEncoder().encodeToString(bytes)
    )
}