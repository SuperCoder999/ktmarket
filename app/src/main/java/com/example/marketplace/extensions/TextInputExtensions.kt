package com.example.marketplace.extensions

import com.google.android.material.textfield.TextInputLayout

val TextInputLayout.isValid: Boolean
    get() {
        val text: String = this.text
        return this.error == null && text.isNotEmpty()
    }

var TextInputLayout.text: String
    get() {
        return this.editText?.text?.toString() ?: ""
    }
    set(value) {
        this.editText?.setText(value)
    }