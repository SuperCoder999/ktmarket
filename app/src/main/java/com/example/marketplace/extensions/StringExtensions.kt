package com.example.marketplace.extensions

fun String.limitWords(limit: Int = 15): String {
    val words: List<String> = split(" ")

    if (words.size <= limit) {
        return this
    }

    return words.subList(0, limit + 1).joinToString(" ") + "\u2026" // Ellipsis
}