package com.example.marketplace.extensions

inline fun <reified T>List<T>.replaceWhere(replacement: T, predicate: T.() -> Boolean): List<T> {
    val newList = mutableListOf(*toTypedArray())

    withIndex().forEach() { (index, it) ->
        if (predicate(it)) {
            newList[index] = replacement
        }
    }

    return newList
}

inline fun <reified T>List<T>.replaceWhere(getReplacement: T.() -> T, predicate: T.() -> Boolean): List<T> {
    val newList = mutableListOf(*toTypedArray())

    withIndex().forEach() { (index, it) ->
        if (predicate(it)) {
            newList[index] = getReplacement(it)
        }
    }

    return newList
}

inline fun <reified T>List<T>.removeWhere(predicate: T.() -> Boolean): List<T> {
    val newList = mutableListOf(*toTypedArray())
    var removedCount = 0

    withIndex().forEach() { (index, it) ->
        if (predicate(it)) {
            newList.removeAt(index - removedCount)
            removedCount++
        }
    }

    return newList
}