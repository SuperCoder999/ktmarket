package com.example.marketplace.validation

import android.text.Editable
import android.text.TextWatcher
import com.google.android.material.textfield.TextInputLayout

class InputValidationWatcher(
    private val validator: Validator,
    private val inputLayout: TextInputLayout
) : TextWatcher {
    // Stubs
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    override fun afterTextChanged(editable: Editable?) {
        val text: String = editable?.toString() ?: ""
        val error: String? = validator(text)

        inputLayout.error = error
    }
}