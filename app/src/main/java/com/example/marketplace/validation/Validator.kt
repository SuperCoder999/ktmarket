package com.example.marketplace.validation

typealias Validator = (value: String) -> String?