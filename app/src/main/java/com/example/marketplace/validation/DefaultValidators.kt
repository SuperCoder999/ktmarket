package com.example.marketplace.validation

object DefaultValidators {
    val exists: Validator = { value: String ->
        val valid: Boolean = value.isNotEmpty()

        if (valid) null else "Mustn't be empty"
    }

    val email: Validator = { value: String ->
        val valid: Boolean = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\$"
            .toRegex()
            .matches(value)

        if (valid) null else "Must be a valid email"
    }

    val phoneNumber: Validator = { value: String ->
        val valid: Boolean = "^\\+\\d{12}\$"
            .toRegex()
            .matches(value)

        if (valid) null else "Must be a valid phone number"
    }

    val min8: Validator = { value: String ->
        val valid: Boolean = value.length >= 8
        if (valid) null else "Must be at least 8 characters long"
    }

    val positive: Validator = { value: String ->
        val double: Double? = value.toDoubleOrNull()
        val valid: Boolean = if (double == null) false else double > 0
        if (valid) null else "Must be positive"
    }
}