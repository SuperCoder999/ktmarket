package com.example.marketplace.repositories

import com.example.marketplace.dto.products.ProductDto
import com.example.marketplace.dto.products.ProductListDto
import com.example.marketplace.graphql.GraphQLProvider
import com.example.marketplace.graphql.queries.CartQueries
import org.json.JSONArray
import org.json.JSONObject

object CartRepository {
    suspend fun getCart(): List<ProductListDto>? {
        val response: JSONObject = GraphQLProvider.client.send(CartQueries.getCart)
            ?: return null

        if (response.isNull("cart")) {
            return null
        }

        val cartArray: JSONArray = response.getJSONArray("cart")

        return List(cartArray.length()) { index ->
            ProductListDto.fromJson(cartArray.getJSONObject(index))
        }
    }

    suspend fun addToCart(id: String): ProductDto? {
        val response: JSONObject = GraphQLProvider.client.send(
            CartQueries.addToCart,
            mapOf("id" to id)
        ) ?: return null

        if (response.isNull("addToCart")) {
            return null
        }

        return ProductDto.fromJson(response.getJSONObject("addToCart"))
    }

    suspend fun removeFromCart(id: String): String? {
        val response: JSONObject = GraphQLProvider.client.send(
            CartQueries.removeFromCart,
            mapOf("id" to id)
        ) ?: return null

        if (response.isNull("removeFromCart")) {
            return null
        }

        return response.getString("removeFromCart")
    }
}