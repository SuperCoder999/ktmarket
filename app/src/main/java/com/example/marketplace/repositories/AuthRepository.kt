package com.example.marketplace.repositories

import com.example.marketplace.dto.auth.UserCredentialsDto
import com.example.marketplace.dto.auth.UserDto
import com.example.marketplace.dto.auth.UserRegisterDto
import com.example.marketplace.graphql.GraphQLProvider
import com.example.marketplace.graphql.queries.AuthQueries
import com.example.marketplace.storage.LocalStorage
import com.example.marketplace.viewModels.ProductsViewModel
import com.example.marketplace.viewModels.ViewModelGetter
import org.json.JSONObject

object AuthRepository {
    suspend fun getUser(): UserDto? {
        val response: JSONObject = GraphQLProvider.client.send(AuthQueries.getUser) ?: return null

        if (response.isNull("user")) {
            return null
        }

        return UserDto.fromJson(response.getJSONObject("user"))
    }

    suspend fun login(data: UserCredentialsDto): UserDto? {
        val response: JSONObject = GraphQLProvider.client.send(
            AuthQueries.login,
            data.toMap()
        ) ?: return null

        if (response.isNull("login")) {
            return null
        }

        val token: String = response
            .getJSONObject("login")
            .getString("token")

        LocalStorage.write(LocalStorage.Keys.userToken, token)
        return UserDto.fromJson(response.getJSONObject("login"))
    }

    suspend fun register(data: UserRegisterDto): UserDto? {
        val response: JSONObject = GraphQLProvider.client.send(
            AuthQueries.register,
            data.toMap()
        ) ?: return null

        if (response.isNull("register")) {
            return null
        }

        val token: String = response
            .getJSONObject("register")
            .getString("token")

        LocalStorage.write(LocalStorage.Keys.userToken, token)
        return UserDto.fromJson(response.getJSONObject("register"))
    }

    suspend fun logout() {
        LocalStorage.remove(LocalStorage.Keys.userToken)
        ViewModelGetter.forceRemoveInstance(ProductsViewModel::class.java)
    }
}