package com.example.marketplace.repositories

import com.example.marketplace.dto.products.*
import com.example.marketplace.graphql.GraphQLProvider
import com.example.marketplace.graphql.queries.ProductQueries
import org.json.JSONObject

object ProductRepository {
    suspend fun getProducts(filter: ProductFilterDto): ProductListInfoDto? {
        val response: JSONObject = GraphQLProvider.client.send(
            ProductQueries.getProducts,
            filter.toMap()
        ) ?: return null

        if (response.isNull("products")) {
            return null
        }

        return ProductListInfoDto.fromJson(response.getJSONObject("products"))
    }

    suspend fun getProduct(id: String): ProductDto? {
        val response: JSONObject = GraphQLProvider.client.send(
            ProductQueries.getProduct,
            mapOf("id" to id)
        ) ?: return null

        if (response.isNull("product")) {
            return null
        }

        return ProductDto.fromJson(response.getJSONObject("product"))
    }

    suspend fun create(data: ProductCreateDto): ProductDto? {
        val response: JSONObject = GraphQLProvider.client.send(
            ProductQueries.create,
            data.toMap(),
        ) ?: return null

        if (response.isNull("createProduct")) {
            return null
        }

        return ProductDto.fromJson(response.getJSONObject("createProduct"))
    }

    suspend fun update(data: ProductUpdateDto): ProductDto? {
        val response: JSONObject = GraphQLProvider.client.send(
            ProductQueries.update,
            data.toMap()
        ) ?: return null

        if (response.isNull("updateProduct")) {
            return null
        }

        return ProductDto.fromJson(response.getJSONObject("updateProduct"))
    }

    suspend fun delete(id: String): String? {
        val response: JSONObject = GraphQLProvider.client.send(
            ProductQueries.delete,
            mapOf("id" to id)
        ) ?: return null

        if (response.isNull("deleteProduct")) {
            return null
        }

        return response.getString("deleteProduct")
    }
}