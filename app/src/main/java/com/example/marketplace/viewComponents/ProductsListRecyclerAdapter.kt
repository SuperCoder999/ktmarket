package com.example.marketplace.viewComponents

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.marketplace.R
import com.example.marketplace.dto.products.ProductListDto
import com.example.marketplace.extensions.limitWords
import com.example.marketplace.helpers.ImageHelper
import com.example.marketplace.viewModels.AuthViewModel
import com.example.marketplace.viewModels.ViewModelDelegate

class ProductsListRecyclerAdapter(
    context: Context,
    private val resources: Resources,
    var products: List<ProductListDto>,
    private val details: (id: String) -> Unit,
    private val addToCart: (id: String) -> Unit,
    private val removeFromCart: (id: String) -> Unit
) : RecyclerView.Adapter<ProductsListRecyclerAdapter.ViewHolder>() {
    private val authViewModel by ViewModelDelegate(AuthViewModel::class.java)
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.findViewById(R.id.productListItemImage)
        val title: TextView = view.findViewById(R.id.productListItemTitle)
        val price: TextView = view.findViewById(R.id.productListItemPrice)
        val description: TextView = view.findViewById(R.id.productListItemDescription)
        val primaryButton: Button = view.findViewById(R.id.productListItemPrimaryButton)
        val detailsButton: Button = view.findViewById(R.id.productListItemDetailsButton)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = inflater.inflate(R.layout.product_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product: ProductListDto = products[position]

        if (product.picture == null) {
            holder.image.setImageResource(0)
        } else {
            val bitmap: Bitmap? = ImageHelper.getImageFromNetwork(product.picture!!.url)
            holder.image.setImageBitmap(bitmap)
        }

        holder.title.text = product.title
        holder.price.text = "${product.price}$"
        holder.description.text = product.description.limitWords()

        holder.primaryButton.text = resources.getString(
            if (product.isInCart) R.string.remove_from_cart else R.string.add_to_cart
        )

        if (authViewModel.getUser().value?.id == product.sellerId) {
            holder.primaryButton.visibility = View.GONE
        } else {
            holder.primaryButton.visibility = View.VISIBLE

            if (product.isInCart) {
                holder.primaryButton.setOnClickListener { removeFromCart(product.id) }
            } else {
                holder.primaryButton.setOnClickListener { addToCart(product.id) }
            }
        }

        holder.detailsButton.setOnClickListener { details(product.id) }
    }

    override fun getItemCount(): Int {
        return products.size
    }
}