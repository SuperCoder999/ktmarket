package com.example.marketplace.dto.products

data class ProductFilterDto(var page: Int = 1, var search: String = "") {
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "page" to page,
            "search" to search
        )
    }
}
