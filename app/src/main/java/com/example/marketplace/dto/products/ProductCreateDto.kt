package com.example.marketplace.dto.products

import com.esafirm.imagepicker.model.Image
import com.example.marketplace.extensions.toMap

data class ProductCreateDto(
    var title: String,
    var price: Double,
    var description: String,
    var picture: Image?
) {
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "title" to title,
            "price" to price,
            "description" to description,
            "pictures" to if (picture == null) null else listOf(picture!!.toMap())
        )
    }
}