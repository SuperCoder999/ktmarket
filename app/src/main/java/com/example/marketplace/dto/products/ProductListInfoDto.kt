package com.example.marketplace.dto.products

import org.json.JSONArray
import org.json.JSONObject

data class ProductListInfoDto(var hasMoreProducts: Boolean, var products: List<ProductListDto>) {
    companion object {
        fun fromJson(json: JSONObject): ProductListInfoDto {
            val productsArray: JSONArray = json.getJSONArray("products")

            return ProductListInfoDto(
                json.getBoolean("hasMoreProducts"),
                List(productsArray.length()) {
                    ProductListDto.fromJson(productsArray.getJSONObject(it))
                }
            )
        }
    }
}
