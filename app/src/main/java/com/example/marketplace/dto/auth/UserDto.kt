package com.example.marketplace.dto.auth

import org.json.JSONObject

data class UserDto(var id: String) {
    companion object {
        fun fromJson(json: JSONObject): UserDto {
            return UserDto(json.getString("id"))
        }
    }
}