package com.example.marketplace.dto.products

import com.esafirm.imagepicker.model.Image
import com.example.marketplace.extensions.toMap

data class ProductUpdateDto(
    var id: String,
    var title: String,
    var price: Double,
    var description: String,
    var picture: Image?,
    var deletedPicture: String?
) {
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "id" to id,
            "title" to title,
            "price" to price,
            "description" to description,
            "pictures" to if (picture == null) null else listOf(picture!!.toMap()),
            "deletedPictures" to (if (deletedPicture == null) null else listOf(deletedPicture))
        )
    }
}
