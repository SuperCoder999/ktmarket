package com.example.marketplace.dto.products

import com.example.marketplace.dto.pictures.PictureDto
import org.json.JSONObject

data class SellerDto(
    var id: String,
    var name: String,
    var phoneNumber: String,
    var avatar: PictureDto?
) {
    companion object {
        fun fromJson(json: JSONObject): SellerDto {
            return SellerDto(
                json.getString("id"),
                json.getString("name"),
                json.getString("phoneNumber"),
                if (json.isNull("avatar")) null
                else PictureDto.fromJson(json.getJSONObject("avatar"))
            )
        }
    }
}
