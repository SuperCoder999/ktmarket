package com.example.marketplace.dto.products

import com.example.marketplace.dto.pictures.PictureDto
import org.json.JSONArray
import org.json.JSONObject

data class ProductDto(
    var id: String,
    var title: String,
    var price: Double,
    var description: String,
    var isInCart: Boolean,
    var seller: SellerDto,
    var picture: PictureDto?
) {
    companion object {
        fun fromJson(json: JSONObject): ProductDto {
            val pictureArray: JSONArray = json.getJSONArray("pictures")

            return ProductDto(
                json.getString("id"),
                json.getString("title"),
                json.getDouble("price"),
                json.getString("description"),
                json.getBoolean("isInCart"),
                SellerDto.fromJson(json.getJSONObject("seller")),
                if (pictureArray.length() == 0) null
                    else PictureDto.fromJson(pictureArray.getJSONObject(0))
            )
        }
    }
}