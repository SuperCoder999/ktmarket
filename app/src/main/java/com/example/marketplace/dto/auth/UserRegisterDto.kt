package com.example.marketplace.dto.auth

import com.esafirm.imagepicker.model.Image
import com.example.marketplace.extensions.toMap

data class UserRegisterDto(
    var name: String,
    var email: String,
    var phoneNumber: String,
    var password: String,
    var avatar: Image?
) {
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "name" to name,
            "email" to email,
            "phoneNumber" to phoneNumber,
            "password" to password,
            "avatar" to avatar?.toMap()
        )
    }
}