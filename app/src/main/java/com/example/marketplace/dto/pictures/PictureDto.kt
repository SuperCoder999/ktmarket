package com.example.marketplace.dto.pictures

import org.json.JSONObject

data class PictureDto(var id: String, var url: String) {
    companion object {
        fun fromJson(json: JSONObject): PictureDto {
            return PictureDto(
                json.getString("id"),
                json.getString("url")
            )
        }
    }
}
