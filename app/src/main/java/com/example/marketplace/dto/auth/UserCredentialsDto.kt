package com.example.marketplace.dto.auth

data class UserCredentialsDto(var email: String, var password: String) {
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "email" to email,
            "password" to password
        )
    }
}
