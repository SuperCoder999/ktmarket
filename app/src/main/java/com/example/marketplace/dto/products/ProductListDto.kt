package com.example.marketplace.dto.products

import com.example.marketplace.dto.pictures.PictureDto
import org.json.JSONArray
import org.json.JSONObject

data class ProductListDto(
    var id: String,
    var title: String,
    var price: Double,
    var description: String,
    var sellerId: String,
    var isInCart: Boolean,
    var picture: PictureDto?
) {
    companion object {
        fun fromJson(json: JSONObject): ProductListDto {
            val pictureArray: JSONArray = json.getJSONArray("pictures")

            return ProductListDto(
                json.getString("id"),
                json.getString("title"),
                json.getDouble("price"),
                json.getString("description"),
                json.getString("sellerId"),
                json.getBoolean("isInCart"),
                if (pictureArray.length() == 0) null
                    else PictureDto.fromJson(pictureArray.getJSONObject(0))
            )
        }

        fun fromFull(full: ProductDto): ProductListDto {
            return ProductListDto(
                full.id,
                full.title,
                full.price,
                full.description,
                full.seller.id,
                full.isInCart,
                full.picture
            )
        }
    }
}
